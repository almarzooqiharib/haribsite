## Building the FabTinyISP
The objective was to Building the FabTinyISP which is AVR ISP programmer/board that can be produced in a fab lab using a milled PCB and readily available components.
Design reference [here](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/index.html).

## Fabricating ISP

**In order to build the ISP unit, I starting with the Traces:**
Traces pic
1.	Open [Fab Modules](http://fabmodules.org).
2.	Input format: image (.png), and uploaded the traces.
3.	Output format: Roland mill (.rml). (the PCB milling machine name)
4.	Process: PCB traces (1/64).
5.	Set machine: SRM20 (the machine model)
6.	Set:
        * X0 (mm): 0
        * Y0 (mm): 0
        * Z0 (mm): 0

7.	Calculate
8.	Save (save it to USB drive)
For the outline:
Repeated the similar steps but used different Process: PCB outline (1/32).

**Printing preparations**

1. Removed the used copper clad laminate from the MDF board and cleaned it.
2. To attach the new FR1 copper clad laminate I used double side tape.
3. Attached it and made sure that its straight and leveled.

Printing the traces
1.	Installed the drill bit size 1/64 SE.  
2.	Reset the axes starting with X Y, then Z.
3.	Screw the drill bit again so it touch’s the copper laminate.
4.	Clicking on Cut I added the file that I wish to cut "fts_mini_traces.rml".
5.	Pressing output starts the cut.


Printing the outline
1.	Milling the outline requires a drill bit size 1/32 SE.
2.	Clicking on Cut I added the file that I wish to cut "haribi outline.rml".
3.	Pressing output starts the cut.
4.	Finally, remove the PCB piece with a scraper.

## Assembling ISP

Obtain the components:
* 1x ATtiny45 or ATtiny85
* 2x 1kΩ resistors
* 2x 499Ω resistors
* 2x 49Ω resistors
* 2x 3.3v zener diodes
* 1x red LED
* 1x green LED
* 1x 100nF capacitor
* 1x 2x3 pin header

Using the schematic and board image below I soldered the parts to the PCB.

## Programing ISP

## Installing software

_Run the following commands in bash:_
1. ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null
2. brew tap osx-cross/avr
3. brew install gcc
4. brew tap osx-cross/avr && brew install avr-gcc
5. Brew install avrdude
6. avr-gcc --version "This will check the version"
7. make -v

Download the [firmware source code](http://fab.cba.mit.edu/classes/863.16/doc/projects/ftsmin/fts_firmware_bdm_v1.zip) and extract the zip file . Open your terminal program and cd into the source code directory.

bash comands:

1. cd Desktop
2. cd fts_firmware_bdm_v1
3. make "This will build the hex file that will get programmed onto the ATtiny45"

Plug the board into a USB port. Use a USB 2.0 port "The red LED should be lit up". Connect the programmer to the ISP header on your board.
To check if the ISP is plugged on mac go to Apple Menu → About this Mac → system reports → Hardware → USB

bash commands:

1. make flash
>This will erase the target chip, and program its flash memory with the contents of the .hex file you built before

2. make fuses
>This will set up all of the fuses except the >one that disables the reset pin

3. make rstdisbl
>This does the same thing as the make fuses command, but this time it's going to include that reset disable bit as well

### Issues I Faced:

_The broken drill bit:_ half way through printing I found out that it was not printing perfect as it spouses to do. Replaced the bit and all went back to normal.

_Diodes:_ It’s hard to indicate the polarity of the diodes, after soldering them I had to fix one of them by removing the solder with the desoldering wire and resolder it.

_make flash, fuses did not work:_
avrdude: Can't find programmer id "USBtinySPI". did all the process on mentor laptop, meanwhile a class mate found a solution [Shaikha Al Mazaina](https://shaikha_matar.gitlab.io/sam_website/).

_had to use USB hub:_ USB 3.0 on macbook pro is to fast for the ISP had to plug a USB hub.
